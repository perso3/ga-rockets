canvasWidth = 500;
canvasHeight = 500;

let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');
ctx.canvas.width = canvasWidth;
ctx.canvas.height = canvasHeight;

let targetImage = document.getElementById('target');

let tickElement = document.getElementById('tick');
let generationElement = document.getElementById('generation');
let bestScoreElement = document.getElementById('best-score');

let tickRateElement = document.getElementById('tick-rate');
let maxTickElement = document.getElementById('max-tick');
let maxGenerationElement = document.getElementById('max-generation');
let populationSizeElement = document.getElementById('population-size');
let populationOnScreenElement = document.getElementById('population-on-screen');
let mutationRateElement = document.getElementById('mutation-rate');
let elitismRateElement = document.getElementById('elitism-rate');
let startSimulationElement = document.getElementById('start-simulation');
let stopSimulationElement = document.getElementById('stop-simulation');
let wallContainerElement = document.getElementById('wall-container');
let addWallElement = document.getElementById('add-wall');

let wallIndex = 0;

addWallElement.onclick = () => {
    let wall = document.createElement('div');
    wall.className = 'wall';
    wall.id = 'wall-' + wallIndex;
    wallIndex++;

    wall.innerHTML = `            <label>
                X
                <input type="text" class="x">
            </label>
            <label>
                Y
                <input type="text" class="y">
            </label>
            <label>
                W
                <input type="text" class="w">
            </label>
            <label>
                H
                <input type="text" class="h">
            </label>
            <button>Remove</button>
        `;

    wall.querySelector('button').onclick = () => {
        wall.remove();
    }
    wallContainerElement.appendChild(wall);
}

startSimulationElement.onclick = () => {
    clearInterval(simulationLoopId);
    walls = createWalls();
    tickRate = 1000 / parseInt(tickRateElement.value);
    simulationMaxTicks = parseInt(maxTickElement.value);
    simulationMaxTicks = parseInt(maxTickElement.value);
    populationSize = parseInt(populationSizeElement.value);
    childOnScreen = parseInt(populationOnScreenElement.value);
    mutationRate = parseFloat(mutationRateElement.value);
    elitismRate = parseFloat(elitismRateElement.value);
    maxGeneration = parseInt(maxGenerationElement.value);

    actualGeneration = [];
    allGenerations = [];
    overallBestScore = -1;
    generationNumber = 0;
    simulationTick = 0;

    mainLoop();
}
stopSimulationElement.onclick = () => {
    clearInterval(simulationLoopId);
}
let overallBestScore = -1;

let tickRate = 1000 / 300;

let minGeneration = 100;
let maxGeneration = 200;
let mutationRate = 0.01;
let elitismRate = 0.01;

let populationSize = 1000;
let maxGenes = 100;

let startPosition = {
    x: 250,
    y: 450,
}

let goal = {
    x: 250,
    y: 100,
}

let simulationTick = 0;
let simulationMaxTicks = 600;
let simulationLoopId;

let childOnScreen = 100;

let allGenerations = [];

let actualGeneration = [];
let generationNumber = -1;

let walls = [];

class Wall {
    constructor(x, y, width, height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    show() {
        ctx.fillStyle = 'black';
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }

    checkCollision(x, y) {
        return (x > this.x && x < this.x + this.width && y > this.y && y < this.y + this.height);
    }
}

class Child {

    id = guid();

    position = {...startPosition};

    direction = {
        x: 0,
        y: -1
    }

    power = 0;

    genes = [];
    actualGeneNumber = -1;
    tickLeft = -1;
    fitness = -1;
    walkedDistance = 0;
    color = '#' + Math.floor(Math.random() * 16777215).toString(16);
    state = 1;


    mutate() {
        for (let i = 0; i < this.genes.length; i++) {
            if (getRandomInt(0, 100)/100 < mutationRate) {
                this.genes[i] = {
                    direction: this.genes[i].direction + getRandomInt(-10, 10),
                    power: this.genes[i].power + getRandomFloat(-0.1, 0.1),
                    duration: this.genes[i].duration + getRandomInt(-10, 10),
                }
                if (this.genes[i].power < 0.5) {
                    this.genes[i].power = 0.5;
                } else if (this.genes[i].power > 1) {
                    this.genes[i].power = 1;
                }
                if (this.genes[i].duration < 0) {
                    this.genes[i].duration = 0;
                }
            }
        }
    }

    update() {

        if (this.state === 0) {
            return;
        }

        if (this.actualGeneNumber === -1) {
            this.actualGeneNumber = 0;
            this.tickLeft = this.genes[0].duration;
            let directionAngle = vectorToAngle(this.direction) + this.genes[0].direction;
            this.direction = angleToVector(directionAngle);
            this.power = this.genes[0].power;
        }

        this.position.x += this.direction.x * this.power;
        this.position.y += this.direction.y * this.power;

        this.walkedDistance += distanceBetween({x: 0, y: 0}, this.direction) * this.power;

        this.boundaryCheck();

        if (this.tickLeft > 0) {
            this.tickLeft--;
        }
        else {
            this.actualGeneNumber++;
            if (this.actualGeneNumber >= this.genes.length) {
                this.actualGeneNumber = 0;
            }
            this.tickLeft = this.genes[this.actualGeneNumber].duration;
            let directionAngle = vectorToAngle(this.direction) + this.genes[this.actualGeneNumber].direction;
            this.direction = angleToVector(directionAngle);
            this.power = this.genes[this.actualGeneNumber].power;
        }
    }

    boundaryCheck() {
        if (this.position.x < 0) {
            this.state = 0;
        } else if (this.position.x > canvasWidth-10) {
            this.state = 0;
        }

        if (this.position.y < 0) {
            this.state = 0;
        } else if (this.position.y > canvasHeight-10) {
            this.state = 0;
        }

        for (let wall of walls) {
            if (wall.checkCollision(this.position.x, this.position.y)) {
                this.state = 0;
            }
        }
    }

    show() {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.position.x, this.position.y, 10, 10);
    }

    calculateFitness() {
        let distance = distanceBetween(this.position, goal);

        this.fitness = this.state * (1 / distance) * (1 / this.walkedDistance);
    }
}

class Gene {
    direction = getRandomInt(-50, 50);
    power = getRandomFloat(0.5, 1);
    duration = getRandomInt(0, 60);
}

createWalls = () => {
    let newWalls = [];
    let wallElements = wallContainerElement.getElementsByClassName('wall');
    for (let wall of wallElements) {
        let x = parseInt(wall.getElementsByClassName('x')[0].value);
        let y = parseInt(wall.getElementsByClassName('y')[0].value);
        let width = parseInt(wall.getElementsByClassName('w')[0].value);
        let height = parseInt(wall.getElementsByClassName('h')[0].value);
        newWalls.push(new Wall(x, y, width, height));
    }

    return newWalls;
}

guid = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

getRandomFloat = (min, max) => {
    return Math.random() * (max - min) + min;
}

distanceBetween = (a, b) => {
    return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
}

normalize = (vector) => {
    let length = Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2));
    return {
        x: vector.x / length,
        y: vector.y / length
    }
}

vectorToAngle = (vector) => {
    return Math.atan2(vector.y, vector.x) * 180 / Math.PI;
}

angleToVector = (angle) => {
    return {
        x: Math.cos(angle * Math.PI / 180),
        y: Math.sin(angle * Math.PI / 180)
    }
}

createNewGeneration = (prevGeneration) => {
    let newGeneration = [];

    if (prevGeneration.length === 0) {
        for (let i = 0; i < populationSize; i++) {
            let newChild = new Child();
            newChild = fillGenes(newChild);
            newGeneration.push(newChild);
        }
    } else {
        for (const child of prevGeneration) {
            let newChild = new Child();
            newChild.color = child.color;
            newChild.genes = child.genes;
            newGeneration.push(newChild);
        }

        for (let i = 0; i < populationSize - prevGeneration.length; i++) {
            let newChild = new Child();
            let parent1 = prevGeneration[Math.floor(Math.random() * prevGeneration.length)];
            let parent2 = prevGeneration[Math.floor(Math.random() * prevGeneration.length)];

            newChild.genes = parent1.genes.slice(0, parent1.genes.length / 2).concat(parent2.genes.slice(parent2.genes.length / 2, parent2.genes.length));
            newChild.mutate();
            newGeneration.push(newChild);
        }
    }
    return newGeneration;
};

fillGenes = (child) => {
    for (let i = 0; i < maxGenes; i++) {
        child.genes.push(new Gene());
    }

    return child;
}

getBestChildren = (generation) => {
    if (generation.length === 0) {
        return [];
    }

    for (let i = 0; i < generation.length; i++) {
        generation[i].calculateFitness();
    }

    generation.sort((a, b) => {
        return b.fitness - a.fitness;
    });

    if (overallBestScore === -1 || generation[0].fitness > overallBestScore) {
        overallBestScore = generation[0].fitness;
        bestScoreElement.innerHTML = "Best score: " + overallBestScore;
    }

    return generation.splice(0, Math.floor(generation.length * elitismRate));
}

simulationLoop = () => {
    tickElement.innerHTML = "Tick: " + simulationTick;
    if (simulationTick < simulationMaxTicks) {
        simulate();
        simulationTick++;
    }
    else {
        simulationTick = 0;
        clearInterval(simulationLoopId);
        mainLoop();
    }
}

mainLoop = () => {
    if (generationNumber < maxGeneration) {
        generationNumber++;
        generationElement.innerHTML = "Generation: " + generationNumber;
        let bestChildren = getBestChildren(actualGeneration);
        actualGeneration = createNewGeneration(bestChildren);
        allGenerations.push(actualGeneration);
        simulationLoopId = setInterval(simulationLoop, tickRate);
    }
}

simulate = () => {
    ctx.clearRect(0, 0, canvasWidth, canvasHeight);
    ctx.drawImage(targetImage, goal.x - targetImage.width / 2, goal.y - targetImage.height / 2);

    for (const wall of walls) {
        wall.show();
    }

    let count = 0;
    for (let child of actualGeneration) {
        if (child.state === 1) {
            child.update();
        }

        if (count < childOnScreen) {
            child.show()
        }
        count++;
    }
}

